class MyList:
    def __isnumber(self, n):
        if not isinstance(n, (int, float, complex)):
            return False
        return True
    def __init__(self, *args):
        for arg in args:
            if not self.__isnumber(arg):
                print('所有的元素必须是数字类型')
                return
        self.__value = list(args)
    def __str__(self):
        return str(self.__value)
    def __del__(self):
        del self.__value
    def __add__(self, num):
        if self.__isnumber(num):
            # 数组中所有元素都与数字num相加
            my_list = MyList()
            my_list.__value = [elem + num for elem in self.__value]
            return my_list
    def __sub__(self, num):
        if not self.__isnumber(num):
            print('所有的元素必须是数字类型')
            return
        my_list = MyList()
        my_list.__value = [elem - num for elem in self.__value]
        return my_list
    def __mul__(self, num):
        if not self.__isnumber(num):
            print('所有的元素必须是数字类型')
            return
        my_list = MyList()
        my_list.__value = [elem * num for elem in self.__value]
        return my_list
    def __truediv__(self, num):
        if not self.__isnumber(num):
            print('所有的元素必须是数字类型')
            return
        my_list = MyList()
        my_list.__value = [elem / num for elem in self.__value]
        return my_list