import pygame
import random

SCREEN_RECT=pygame.Rect(0,0,480,700)
RAME_INTERVAL =10

HERO_BOMB_COUNT=3
HERO_DEFAULT_MID_BOTTOM=(SCREEN_RECT.CENTERX,SCREEN_RECT.bottom-99)


class GameSprite(pygame.sprite.Sprite):
    """游戏精灵类"""
    res_path="./res/images/"
    def __init__(self, image_name, speed, *groups):
        """构造方法
        :param image_name:要加载的图片文件名
        :param speed:移动速度,0表示静止
        :param groups:要添加到的精灵组,不传则不添加
        """
        super().__init__(*groups)
        self.image=pygame.image.load(self.res_path + image_name)
        self.rect=self.image.get_rect()
        self.speed=speed
    
    def update(self,*args):
        """更新精灵位置，默认在垂直方向移动"""
        self.rect.y+=self.speed


class Background(GameSprite):
            """背景精灵类"""
            def __init__(self,is_alt, *groups):
                super().__init__init("background.png",1,*groups)
                if is_alt:
                    self.rect.y=-self.rect.h

            def update(self,*arges):
                super().update(*arges)
                if self.rect.y>=self.rect.h:
                    self.rect.y=-self.rect.h
class StatusBUtton(GameSprite):
            """状态按钮类"""
            def __init__(self,images_names,*groups):
                """构造方法
                :param image_names:要加载的图像名称列表
                :param groups:要添加到的精灵组
                """
                super().__init__(images_names[0],0,*groups)
                self.images=[pygame.image.load(self.res_path+name) for name in images_names]
            
            def switch_status(self,is_pause):
                """切换状态
                :param is_pause :是否暂停
                """
                self.image=self.images[1 if is_pause else 0]
        

class Label(pygame.sprite.Sprite):
            """文本标签精灵"""
            font_path="./res/font/MarkerFelt.ttc"

            def __init__(self,text,size,color,*groups):
                super().__init__(*groups)

                self.font=pygame.font.Font(self.font_path,size)
                self.color=color
                self.image=self.font.render(text,True,self.color)
                self.rect=self.image.get_rect()

            def set_text(self,text):
                self.image=self.font.render(text,True,self.color)
                self.rect=self.image.get_rect()

class Plane(GameSprite):
    '''飞机类'''
    def __init__(self,hp,speed,value,wav_name,normal_names,hurt_name,destory_names,*groups):
        super().__init__(normal_names[0],0,*groups)
        self.hp=hp 
        self.max_hp=hp 
        self.value=value 
        self.wav_name=wav_name
        self.normal_images=[pygame.image.load(self.res_path+name)for name in normal_names]
        self.normal_index=0
        self.hurt_image=pygame.image.load(self.res_path+hurt_name)
        self.destory_images=[pygame.image.load(self.res_path+name)for name in destory_names]
        self.destory_index=0

    def update(self,*args):
        if not args[0]:
            return
        self.image=self.normal_images[self.normal_index]
        count=len(self.normal_images)
        self.normal_index=(self.normal_index+1)%count   

    def rest_plane(self):
        """重置飞机"""
        self.hp=self.max_hp
        self.normal_index=0
        self.destory_index=0
        self.image=self.normal_images[0]
    def update(self,*args):
        if not args[0]:
            return
        if self.hp==self.max_hp:
            self.image=self.normal_images[self.normal_index]
            count=len(self.normal_images)
            self.normal_index(self.normal_index+1)%count
        elif self.hp>0:
            self.image=self.hurt_image
        else:
            if self.destory_index<len(self.destory_images):
                self.image=self.destory_images[self.destory_index]
                self.destory_index+=1
            else:
                self.reset_plane() 

class Enemy(Plane):
    """敌机类"""
    def __init__(self,kind,max_speed,*groups):
        self.kind=kind
        self.max_speed=max_speed
        if kind ==0:
            super().__init__(1,1,1000,"enemy1_down.wav",["enemy1,png"],"enemy1.phg",["enemy1_down%d.png"% i for i in range(1,5)],*groups)
        elif kind ==1:
            super().__init__(6,1,6000,"enemy2_down.wav",["enemy2.png"],"enemy2_hit.png",["enemy2_down%d.png"% i for i in range(1,5)],*groups)
        else:
            super().__init__(15,1,15000,"enemy3_down.wav",["enemy3_n1.png","enemy3_n2.png"],"enemy3_hit.png",["enemy3_down%d.png"% i for i in range(1,7)],*groups)
            
        self.reset_plane()
    def reset_plane(self):
        """重置飞机"""
        super().reset_plane()
        
        x=random.randint(0,SCREEN_RECT.w-self.rect.w)
        y=random.randint(0,SCREEN_RECT.h-self.rect.h)-SCREEN_RECT.h
        self.rect.topleft=(x,y)
        self.speed=random.randint(1,self.max_speed)

    def update(self,*args):
        """更新图像和位置"""
        super().update(*args)
        if self.hp>0:
            self.rect.y +=self.speed
        if self.rect.y>=SCREEN_RECT.h:
            self.reset_plane()
     
class Hero(Plane):
    """英雄类"""
    def __init__(self,*groups):
        super().__init__(1000,5,0,"me_down.wav",["me%d.png"% i for i in range(1,3)],"mel.png",["me_destory_%d.png"% i for i in range(1,5)],*groups)
        self.is_power=False
        self.bomb_count=HERO_BOMB_COUNT
        self.bullets_kind=0
        self.bullets_group=pygame.sprite.Group()
        self.rect.midbottom=HERO_DEFAULT_MID_BOTTOM